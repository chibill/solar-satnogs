# solar-satnogs

Running a satnogs "off the grid" where its own source of energy is solar and/or wind power. Uses batteries when solar or wind is not avaliable. 

## Goal

The main goal of the project is to build an ESP32 based controller that can manage a Satnogs Station that is running of solar / battery power. This means shuting down the raspberry pi when not in use and using the ESP32 to keep the station online on the network and watch for upcoming observations to turn the raspberry pi back on for. 

This will also include setting the station's Target Utilization and also Online status based on battery avaliable. 

Along with running the station the controller will also be tasked with maintaining the system's internal tempature at a desireable level, which may include running heaters during winter or opening dampers during summer. 
