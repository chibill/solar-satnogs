
###Jobs API
```
URL https://network.satnogs.org/api/jobs
Paramaters 
    ground_station: Ground Station ID
    lat: Stations Latitude
    lon: Stations Longitude
    alt: Stations Elevation / Altitude

headers 
    'Authorization': 'Token {SATNOGS_API_TOKEN}'
```


Returns a json list of all futrue jobs your stations and if the header is present updates the timeout of it your station is online or not.

Example on an entry in the list 

```
{
    "id": 7107463,
    "start": "2023-02-03T17:13:53Z",
    "end": "2023-02-03T17:18:21Z",
    "ground_station": 2271,
    "tle0": "0 OPS-SAT",
    "tle1": "1 44878U 19092F   23032.46674150  .00018617  00000-0  72756-3 0  9998",
    "tle2": "2 44878  97.4765 220.2481 0010260 263.3642  96.6428 15.25870908173039",
    "frequency": 437200000,
    "mode": "MSK AX.100 Mode 6",
    "transmitter": "MVCWqc9Xt4JfF36marnJfZ",
    "baud": 9600.0
}
```



Currently no API for setting target utilzation
